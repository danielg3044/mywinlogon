/*

Error Code	Description
0XC000005E	There are currently no logon servers available to service the logon request.
0xC0000064	User logon with misspelled or bad user account
0xC000006A	User logon with misspelled or bad password
0XC000006D	This is either due to a bad username or authentication information
0XC000006E	Unknown user name or bad password.
0xC000006F	User logon outside authorized hours
0xC0000070	User logon from unauthorized workstation
0xC0000071	User logon with expired password
0xC0000072	User logon to account disabled by administrator
0XC00000DC	Indicates the Sam Server was in the wrong state to perform the desired operation.
0XC0000133	Clocks between DC and other computer too far out of sync
0XC000015B	The user has not been granted the requested logon type (aka logon right) at this machine
0XC000018C	The logon request failed because the trust relationship between the primary domain and the trusted domain failed.
0XC0000192	An attempt was made to logon, but the Netlogon service was not started.
0xC0000193	User logon with expired account
0XC0000224	User is required to change password at next logon
0XC0000225	Evidently a bug in Windows and not a risk
0xC0000234	User logon with account locked
0XC00002EE	Failure Reason: An Error occurred during Logon
0XC0000413	Logon Failure: The machine you are logging onto is protected by an authentication firewall. The specified account is not allowed to authenticate to the machine.

*/

function LogonStatus(error) {
    if (error) {
        result.style.color = "#900";

        switch (error) {
            case -1073741730: // 0xC000005E
                result.innerHTML = "Logon server is unavailable";
                break;

            case -1073741715: //0xC000006D
                result.innerHTML = "Wrong user name or password";
                break;

            // ....... 

            default:
                result.innerHTML = "Error: " + error;
        }
    }
    else {
/*
If the credentials are valid, the error code is 0.
Quit the application and the station will be opened in a few seconds.
*/
        result.style.color = "#070";
        result.innerHTML = "Access Granted";

        setTimeout(function () {
            __WinLogon__.Exit();
        }, 2000);
    }
}

function Logon() {
/*
Put your logon logic here.
For the real application there should be additional authentication means that substitute or supplement the user's password.
After the user's data is successfully verified, print out the standard Windows credentials to the console as domain\account\password. 
For the local machine logon use '.' as a domain name.
*/
    if(user.value.length && password.value.length)
        __WinLogon__.Logon(domain.value.length ? domain.value : ".", user.value, password.value);
}