import tkinter as tk
from tkinter import *

canExit = False

def Exit():
    global canExit 
    if canExit:
        close() 

def center(win):
    win.update_idletasks()
    width = win.winfo_width()

    frm_width = win.winfo_rootx() - win.winfo_x()
    win_width = width + 2 * frm_width
    height = win.winfo_height()
    win_height = height + frm_width

    x = win.winfo_screenwidth() // 2 - win_width // 2
    y = win.winfo_screenheight() // 2 - win_height // 2
    win.geometry('{}x{}+{}+{}'.format(width, height, x, y))

window = tk.Tk()
window.overrideredirect(1)
window.attributes("-topmost", True)
center(window)

window.protocol("WM_DELETE_WINDOW", Exit)

def Logon() :
'''
Put your logon logic here.
For the real application there should be additional authentication means that substitute or supplement the user's password.
After the user's data is successfully verified, print out the standard Windows credentials to the console as domain\account\password. 
For the local machine logon use '.' as a domain name.
If the credentials are valid, the string read from the console input is empty.
Quit the application and the station will be opened in a few seconds.
'''

    domain = domainBox.get()
    user = userBox.get()
    password = passwordBox.get()

    if len(user) == 0 or len(password) == 0:
        return

    if len(domain) == 0:
        domain = '.'

    creds = domain + '\\' + user + '\\' + password
    print(creds, flush=True)
    errorText = sys.stdin.readline()
    errorText = errorText.rstrip('\r\n')

    if len(errorText) > 0:
        errVar.set(errorText)
    else:
        canExit = True
        window.destroy()

titleLabel = Label(window, text='Windows Logon', fg='blue')
titleLabel.config(font=("Tahoma", 12))

userLabel = Label(window, text='User')
userBox = Entry(window, textvariable=StringVar())
       
domainLabel = Label(window, text='Domain')
domainBox = Entry(window, textvariable=StringVar())

passwordLabel = Label(window, text='Password')
passwordBox= Entry(window, show='*', textvariable=StringVar())

button = Button(window, text='Logon', command=Logon)

errVar = StringVar()
errLabel = Label(window, textvariable=errVar, fg='red')

titleLabel.pack()
userLabel.pack()
userBox.pack()
domainLabel.pack()
domainBox.pack()
passwordLabel.pack()
passwordBox.pack()
errLabel.pack()
button.pack()

window.mainloop()
