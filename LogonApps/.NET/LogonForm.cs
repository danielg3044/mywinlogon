﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class LogonForm : Form
    {
        private bool exit = false;

        public LogonForm()
        {
            InitializeComponent();
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !exit && e.CloseReason == CloseReason.UserClosing;
        }

        private void logonBtn_Click(object sender, EventArgs e)
        {
            if (userBox.Text.Length > 0 && passwordBox.Text.Length > 0)
            {
                /*
                 * Put your logon logic here.
                 * For the real application there should be additional authentication means that substitute or supplement the user's password.
                 * After the user's data is successfully verified, print out the standard Windows credentials to the console as domain\account\password. 
                 * For the local machine logon use '.' as a domain name.
                 * If the credentials are valid, the string read from the console input is empty.
                 * Quit the application and the station will be opened in a few seconds.
                 */

                string credentials = string.Format(@"{0}\{1}\{2}", domainBox.Text.Length == 0 ? "." : domainBox.Text, userBox.Text, passwordBox.Text);
                Console.Write(credentials);
                string result = Console.ReadLine();

                if (string.IsNullOrEmpty(result))
                {
                    exit = true;
                    Close();
                }
                else errorLabel.Text = result;
            }
        }
    }
}
